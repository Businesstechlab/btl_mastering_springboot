package com.btl.bootcamp.service;

import com.btl.bootcamp.model.YogaUser;
import com.btl.bootcamp.repository.YogaUserRepository;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;


@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class YogaUserServiceTest {

    @Autowired
    YogaUserService yogaUserService;

    @MockBean
    YogaUserRepository yogaUserRepository;

    private YogaUser yogaUser;

    @BeforeAll
    public void beforeAll(){

        yogaUser=new YogaUser();
        yogaUser.setEmail("abc@gmail.com");
        yogaUser.setUsername("Abc");
        yogaUser.setPhoneNumber("9001002002");
        yogaUser.setPassword("abc@123");
        yogaUser.setLastUpdatedDate("2022-03-03 08-00-00");
        yogaUser.setIsDeleted(0);
    }

    @Test
    public void addYogaUserTest(){
        when(yogaUserRepository.save(yogaUser)).thenReturn(yogaUser);
        assertEquals(yogaUser,yogaUserService.addYogaUser(yogaUser));
    }
}
