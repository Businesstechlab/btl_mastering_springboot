package com.btl.bootcamp.controller;

import com.btl.bootcamp.dto.request.YogaUserReqDTO;
import com.btl.bootcamp.dto.response.YogaUserRespDTO;
import com.btl.bootcamp.model.YogaUser;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class YogaUserControllerTest {

    @Autowired
    YogaUserController yogaUserController;

    YogaUserReqDTO yogaUserReqDTO;
    YogaUserRespDTO yogaUserRespDTO;

    @BeforeAll
    public void beforeAll(){
        yogaUserReqDTO=new YogaUserReqDTO();
        yogaUserReqDTO.setEmail("abc@gmail.com");
        yogaUserReqDTO.setUsername("Abc");
        yogaUserReqDTO.setPhoneNumber("9001002002");
        yogaUserReqDTO.setPassword("abc@123");

        yogaUserRespDTO= yogaUserController.addYogaUser(yogaUserReqDTO);
    }

    @Test
    public void addYogaUserTest(){
        assertThat(yogaUserRespDTO.getUsername().equals(yogaUserReqDTO.getUsername()));
    }
}
