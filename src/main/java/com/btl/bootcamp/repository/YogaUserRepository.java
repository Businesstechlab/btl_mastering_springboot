package com.btl.bootcamp.repository;

import com.btl.bootcamp.model.YogaUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface YogaUserRepository extends JpaRepository<YogaUser,Long> {

    Optional<YogaUser> findByEmailAndIsDeleted(String email, int isDeleted);

    Optional<YogaUser> findByUserId(UUID id);
}
