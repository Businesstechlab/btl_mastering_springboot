package com.btl.bootcamp.repository;

import com.btl.bootcamp.model.YogaMember;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface YogaMemberRepository extends JpaRepository<YogaMember,Long> {
    Optional<YogaMember> findByMemberId(String memberId);
}
