package com.btl.bootcamp.repository;

import com.btl.bootcamp.model.EventBooking;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface EventBookingRepository extends JpaRepository<EventBooking,Long> {

    Optional<EventBooking> findByEventBookingId(String eventBookingId);
}
