package com.btl.bootcamp.repository;

import com.btl.bootcamp.model.Event;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface EventRepository extends JpaRepository<Event,Long> {
    
    Optional<Event> findByEventId(String eventId);
}
