package com.btl.bootcamp.interceptor;

import org.springframework.stereotype.Component;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Component
public class DemoInterceptor extends HandlerInterceptorAdapter {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        System.out.println("----------------------preHandle method initialized------------------");
        long startTime= System.currentTimeMillis();
        request.setAttribute("startTime",startTime);
        System.out.println("Start Time: "+startTime +" ms");
        System.out.println("----------------------preHandle method ended-------------------------");
        return true;}

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        System.out.println("----------------------afterCompletion method initialized------------------");
        long endTime= System.currentTimeMillis();
        long startTime= Long.parseLong(request.getAttribute("startTime")+"");
        System.out.println("End Time: "+endTime +" ms");
        System.out.println("Total time to process request in millisecond: "+ (endTime-startTime) + " ms");
        System.out.println("----------------------afterCompletion method ended-------------------------");
    }
}
