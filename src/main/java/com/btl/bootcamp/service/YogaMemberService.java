package com.btl.bootcamp.service;

import com.btl.bootcamp.model.YogaMember;
import com.btl.bootcamp.repository.YogaMemberRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.NoSuchElementException;
import java.util.Optional;

@Service
public class YogaMemberService {

    private final int DELETED= 1;

    @Autowired
    YogaMemberRepository yogaMemberRepository;

    public YogaMember addYogaMember(YogaMember yogaMember) {
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String lastUpdatedDate=formatter.format(new Date(System.currentTimeMillis())).toString();
        yogaMember.setLastUpdatedDate(lastUpdatedDate);
        return yogaMemberRepository.save(yogaMember);
    }

    public YogaMember getMemberById(String memberId) {
        return yogaMemberRepository.findByMemberId(memberId).get();
    }

    public void deleteMember(String memberId){
        Optional<YogaMember> yogaMember= yogaMemberRepository.findByMemberId(memberId);
        if(yogaMember.isEmpty()){
            throw new NoSuchElementException();
        }
        YogaMember yogaMember1= yogaMember.get();
        yogaMember1.setIsDeleted(DELETED);
        yogaMemberRepository.save(yogaMember1);
    }
}
