package com.btl.bootcamp.service;

import com.btl.bootcamp.model.YogaUser;
import com.btl.bootcamp.repository.YogaUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityExistsException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.UUID;

@Service
public class YogaUserService {

    private final int NOT_DELETED = 0;

    @Autowired
    YogaUserRepository yogaUserRepository;

    public YogaUser addYogaUser(YogaUser yogaUser) {
        if(yogaUserRepository.findByEmailAndIsDeleted(yogaUser.getEmail(),NOT_DELETED).isPresent()){
            throw new EntityExistsException("User is present already");
        }
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String lastUpdatedDate=formatter.format(new Date(System.currentTimeMillis())).toString();
        yogaUser.setLastUpdatedDate(lastUpdatedDate);
        return yogaUserRepository.save(yogaUser);
    }

    public YogaUser getUserById(UUID id) {
        Optional<YogaUser> yogaUser= yogaUserRepository.findByUserId(id) ;
        if(yogaUser.isEmpty()){throw new NoSuchElementException();}
        return yogaUser.get();
    }

    public void deleteUser(UUID id) {
        Optional<YogaUser> yogaUser=yogaUserRepository.findByUserId(id);
        if(yogaUser.isEmpty()){
            throw new NoSuchElementException();
        }
        yogaUserRepository.delete(yogaUser.get());
    }
}
