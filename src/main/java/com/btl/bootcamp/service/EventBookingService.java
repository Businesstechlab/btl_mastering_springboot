package com.btl.bootcamp.service;

import com.btl.bootcamp.model.Event;
import com.btl.bootcamp.model.EventBooking;
import com.btl.bootcamp.model.YogaMember;
import com.btl.bootcamp.repository.EventBookingRepository;
import com.btl.bootcamp.repository.EventRepository;
import com.btl.bootcamp.repository.YogaMemberRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

@Service
public class EventBookingService {

    @Autowired
    EventBookingRepository eventBookingRepository;

    @Autowired
    YogaMemberRepository yogaMemberRepository;

    @Autowired
    EventRepository eventRepository;

    public YogaMember getYogaMemberByMemberId(String memberId) {
        Optional<YogaMember> member= yogaMemberRepository.findByMemberId(memberId);
        if(member.isEmpty()){
            throw new NoSuchElementException();
        }
        return member.get();
    }

    public Event getEventByEventId(String eventId) {
        Optional<Event> event= eventRepository.findByEventId(eventId);
        if(event.isEmpty()){
            throw new NoSuchElementException();
        }
        return event.get();
    }

    public EventBooking addEventBooking(EventBooking eventBooking, String eventId, String memberId) {

        Event event = getEventByEventId(eventId);
        eventBooking.setEvent(event);
        YogaMember yogaMember= getYogaMemberByMemberId(memberId);
        eventBooking.setYogaMember(yogaMember);

        return eventBookingRepository.save(eventBooking);
    }

    public EventBooking getBookingById(String eventBookingId) {
        return eventBookingRepository.findByEventBookingId(eventBookingId).get();
    }

    public void deleteEventBooking(String eventBookingId) {
        Optional<EventBooking> eventBooking=eventBookingRepository.findByEventBookingId(eventBookingId);
        if(eventBooking.isEmpty()){
            throw new NoSuchElementException();
        }
        eventBookingRepository.delete(eventBooking.get());
    }
}
