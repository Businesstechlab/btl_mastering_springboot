package com.btl.bootcamp.service;

import com.btl.bootcamp.model.Event;
import com.btl.bootcamp.repository.EventRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

@Service
public class EventService {

    @Autowired
    EventRepository eventRepository;

    public Event addEvent(Event event) {
        return eventRepository.save(event);
    }

    public List<Event> getAllEvents() {
        return eventRepository.findAll();
    }

    public void deleteEvent(String eventId) {
        Optional<Event> event=eventRepository.findByEventId(eventId);
        if(event.isEmpty()){
            throw new NoSuchElementException();
        }
        eventRepository.delete(event.get());
    }
}
