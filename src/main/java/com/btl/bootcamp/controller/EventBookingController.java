package com.btl.bootcamp.controller;

import com.btl.bootcamp.dto.request.EventBookingReqDTO;
import com.btl.bootcamp.dto.response.EventBookingRespDTO;
import com.btl.bootcamp.dto.response.EventRespDTO;
import com.btl.bootcamp.model.Event;
import com.btl.bootcamp.model.EventBooking;
import com.btl.bootcamp.service.EventBookingService;
import io.swagger.annotations.ApiOperation;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class EventBookingController {

    @Autowired
    ModelMapper modelMapper;

    @Autowired
    EventBookingService eventBookingService;

    @PostMapping("/event-booking/{eventId}")
    @ApiOperation(value = "Adding an Event Booking")
    public EventBookingRespDTO addEventBooking(@RequestBody EventBookingReqDTO eventBookingReqDTO, @PathVariable ("eventId")
                                               String eventId, @RequestParam String memberId){
        EventBooking eventBooking= modelMapper.map(eventBookingReqDTO, EventBooking.class);
        return modelMapper.map(eventBookingService.addEventBooking(eventBooking,eventId,memberId),EventBookingRespDTO.class);
    }

    @GetMapping("/event-booking/{eventBookingId}")
    @ApiOperation(value = "Getting Event Bookings By Id")
    public EventBookingRespDTO getEventBookingById(@PathVariable ("eventBookingId") String eventBookingId){

        return modelMapper.map(eventBookingService.getBookingById(eventBookingId),EventBookingRespDTO.class);
    }

    @DeleteMapping("/evnt-booking/{eventBookingId}")
    @ApiOperation(value = "Deleteing a Booking ")
    public void deleteEventBooking(@PathVariable ("eventBookingId") String eventBookingId){
        eventBookingService.deleteEventBooking(eventBookingId);
    }
}
