package com.btl.bootcamp.controller;

import com.btl.bootcamp.dto.request.AuthReqDTO;
import com.btl.bootcamp.dto.response.AuthRespDTO;
import com.btl.bootcamp.service.MyUserDetailService;
import com.btl.bootcamp.util.JwtUtil;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
public class JwtController {

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    MyUserDetailService myUserDetailsService;

    @Autowired
    JwtUtil jwtUtil;

    @PostMapping("/authenticate")
    @ApiOperation(value = "Authenticating User")
    public ResponseEntity<?> createAuthToken(@RequestBody AuthReqDTO authReq) throws Exception {
        log.info("Request receive : [" + authReq + "]");

        try {
            authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(authReq.getUsername(), authReq.getPassword()));
        } catch (BadCredentialsException e) {
            throw new Exception("Incorrect Username or Password", e);
        }

        final UserDetails userDetails = myUserDetailsService
                .loadUserByUsername(authReq.getUsername());

        final String jwt = jwtUtil.generateToken(userDetails);

        return ResponseEntity.ok(new AuthRespDTO(jwt));
    }
}
