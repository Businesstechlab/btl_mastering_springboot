package com.btl.bootcamp.controller;

import com.btl.bootcamp.dto.request.EventReqDTO;
import com.btl.bootcamp.dto.response.EventRespDTO;
import com.btl.bootcamp.model.Event;
import com.btl.bootcamp.service.EventService;
import io.swagger.annotations.ApiOperation;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
public class EventController {

    @Autowired
    EventService eventService;

    @Autowired
    ModelMapper modelMapper;

    @PostMapping("/event")
    @ApiOperation(value = "Add an Event")
    public EventRespDTO addEvent(@RequestBody EventReqDTO eventReqDTO){
        Event event= modelMapper.map(eventReqDTO,Event.class);
        return modelMapper.map(eventService.addEvent(event),EventRespDTO.class);
    }

    @GetMapping("/event")
    @ApiOperation(value = "Getting all the Events ")
    public List<EventRespDTO> getAllEvents(){
        return eventService.getAllEvents()
                .stream()
                .map(event -> modelMapper.map(event,EventRespDTO.class))
                .collect(Collectors.toList());

    }

    @DeleteMapping("event/{eventId}")
    @ApiOperation(value = "Delete an Event")
    public void deleteEvent(@PathVariable ("eventId") String eventId){
        eventService.deleteEvent(eventId);
    }
}
