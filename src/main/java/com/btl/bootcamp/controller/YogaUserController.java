package com.btl.bootcamp.controller;

import com.btl.bootcamp.dto.request.YogaUserReqDTO;
import com.btl.bootcamp.dto.response.YogaUserRespDTO;
import com.btl.bootcamp.model.YogaUser;
import com.btl.bootcamp.service.YogaUserService;
import io.swagger.annotations.ApiOperation;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.UUID;

@RestController
public class YogaUserController {

    @Autowired
    YogaUserService yogaUserService;

    @Autowired
    ModelMapper modelMapper;

    @PostMapping("/yogauser")
    @ApiOperation(value = "Adding a Yoga User")
    public YogaUserRespDTO addYogaUser(@Valid @RequestBody YogaUserReqDTO yogaUserReqDTO){
        YogaUser yogaUser= modelMapper.map(yogaUserReqDTO,YogaUser.class);
        return modelMapper.map(yogaUserService.addYogaUser(yogaUser),YogaUserRespDTO.class);
    }

    @GetMapping("/yogauser/{id}")
    @ApiOperation(value = "Getting a Yoga User by their Id")
    public YogaUserRespDTO getYogaUserById(@PathVariable ("id") UUID id){
        return modelMapper.map(yogaUserService.getUserById(id),YogaUserRespDTO.class);
    }

    @DeleteMapping("/yogauser")
    @ApiOperation(value = "Deleting a yoga user")
    public void deleteYogaUser(@RequestParam UUID id){
        yogaUserService.deleteUser(id);
    }
}
