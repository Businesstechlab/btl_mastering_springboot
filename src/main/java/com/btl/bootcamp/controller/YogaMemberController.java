package com.btl.bootcamp.controller;

import com.btl.bootcamp.dto.request.YogaMemberReqDTO;
import com.btl.bootcamp.dto.response.YogaMemberRespDTO;
import com.btl.bootcamp.model.YogaMember;
import com.btl.bootcamp.service.YogaMemberService;
import io.swagger.annotations.ApiOperation;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class YogaMemberController {

    @Autowired
    YogaMemberService yogaMemberService;

    @Autowired
    ModelMapper modelMapper;

    @PostMapping("/member")
    @ApiOperation(value = "Add a Yoga Member")
    public YogaMemberRespDTO addYogaMember(@RequestBody YogaMemberReqDTO yogaMemberReqDTO){
        YogaMember yogaMember= modelMapper.map(yogaMemberReqDTO,YogaMember.class);
        return modelMapper.map(yogaMemberService.addYogaMember(yogaMember),YogaMemberRespDTO.class);
    }

    @GetMapping("/member")
    @ApiOperation("Getting a member by Member Id")
    public YogaMemberRespDTO getMemberById(@RequestParam String memberId){
        return modelMapper.map(yogaMemberService.getMemberById(memberId),YogaMemberRespDTO.class);
    }

    @DeleteMapping("/member")
    @ApiOperation(("Deleting a Yoga member"))
    public void deleteMember(@RequestParam String memberId){
        yogaMemberService.deleteMember(memberId);
    }
}
