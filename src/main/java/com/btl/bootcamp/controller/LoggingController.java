package com.btl.bootcamp.controller;

import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
public class LoggingController {

    @GetMapping("/log")
    @ApiOperation(value = "Checking different log levels")
    public String getLogs(){
        log.error("Error!");
        log.warn("Warning!!");
        log.info("Information");
        log.debug("Debug");
        log.trace("Trace");
        return "Logs generated";
    }
}
