package com.btl.bootcamp.model;

import lombok.Data;
import lombok.experimental.Accessors;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "yoga_members")
@Data
@Accessors(chain = true)
public class YogaMember {

    @Id
    @GenericGenerator(name = "uuid2-generator", strategy = "uuid2")
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "uuid2-generator")
    @Column(length = 36)
    private String memberId;

    private String memberName;
    private String password;
    private String email;
    private String phoneNumber;

    @Column(columnDefinition="DATETIME", nullable = false)
    @DateTimeFormat(style = "yyyy-MM-dd HH:mm:ss")
    private String lastUpdatedDate;

    private int isDeleted;

    @OneToMany(mappedBy = "yogaMember")
    private List<EventBooking> eventBookings;

}
