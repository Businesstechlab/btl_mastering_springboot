package com.btl.bootcamp.model;

import lombok.Data;
import lombok.experimental.Accessors;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import java.util.UUID;

@Entity
@Table(name = "yoga_users")
@Data
@Accessors(chain = true)
public class YogaUser {

    @Id
    @GenericGenerator(name = "uuid2-generator", strategy = "uuid2")
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "uuid2-generator")
    @Type(type ="org.hibernate.type.UUIDCharType")
    @Column(length = 36)
    private UUID userId;

    @NotNull
    private String username;
    private String password;

    //verifies the correct format of email
    @Email
    private String email;
    private String phoneNumber;

    @Column(columnDefinition="DATETIME", nullable = false)
    @DateTimeFormat(style = "yyyy-MM-dd HH:mm:ss")
    private String lastUpdatedDate;


    private int isDeleted;

}
