package com.btl.bootcamp.model;

import lombok.Data;
import lombok.experimental.Accessors;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "event_bookings")
@Data
@Accessors(chain = true)
public class EventBooking {

    @Id
    @GenericGenerator(name = "uuid2-generator", strategy = "uuid2")
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "uuid2-generator")
    @Column(length = 36)
    private String eventBookingId;

    @Column(columnDefinition="DATETIME", nullable = false)
    @DateTimeFormat(style = "yyyy-MM-dd HH:mm:ss")
    private String eventBookingDate;

    @Column(nullable = false)
    private String eventType;

    private String description;

    @ManyToOne
    @JoinColumn(name = "memberId", insertable = false, updatable = false)
    private YogaMember yogaMember;

    @ManyToOne
    @JoinColumn(name = "eventId", insertable = false, updatable = false)
    private Event event;
}
