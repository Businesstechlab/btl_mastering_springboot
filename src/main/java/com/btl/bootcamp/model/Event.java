package com.btl.bootcamp.model;

import lombok.Data;
import lombok.experimental.Accessors;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "events")
@Data
@Accessors(chain = true)
public class Event {

    @Id
    @GenericGenerator(name = "uuid2-generator", strategy = "uuid2")
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "uuid2-generator")
    @Column(length = 36)
    private String eventId;

    @Column(nullable = false)
    private String eventName;

    @Column(nullable = false)
    private String eventType;

    @Column(columnDefinition="DATETIME", nullable = false)
    @DateTimeFormat(style = "yyyy-MM-dd HH:mm:ss")
    private String startDate;

    @Column(columnDefinition="DATETIME", nullable = false)
    @DateTimeFormat(style = "yyyy-MM-dd HH:mm:ss")
    private String endDate;

    private String description;

    @OneToMany(mappedBy = "event")
    private List<EventBooking> eventBookings;
}
