package com.btl.bootcamp.dto.request;

import lombok.Data;

@Data
public class EventReqDTO {

    private String eventName;
    private String eventType;
    private String startDate;
    private String endDate;
    private String description;
}
