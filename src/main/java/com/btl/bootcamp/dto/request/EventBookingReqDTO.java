package com.btl.bootcamp.dto.request;

import lombok.Data;


@Data
public class EventBookingReqDTO {

    private String eventBookingDate;
    private String eventType;
    private String description;
}
