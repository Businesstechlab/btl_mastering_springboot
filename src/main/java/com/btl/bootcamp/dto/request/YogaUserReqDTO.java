package com.btl.bootcamp.dto.request;

import lombok.Data;

@Data
public class YogaUserReqDTO {

    private String username;
    private String password;
    private String email;
    private String phoneNumber;
}
