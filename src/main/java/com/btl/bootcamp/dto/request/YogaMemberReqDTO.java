package com.btl.bootcamp.dto.request;

import lombok.Data;

@Data
public class YogaMemberReqDTO {

    private String memberName;
    private String password;
    private String email;
    private String phoneNumber;

}
