package com.btl.bootcamp.dto.response;

import lombok.Data;

@Data
public class EventRespDTO {

    private String eventId;
    private String eventName;
    private String eventType;
    private String startDate;
    private String endDate;
    private String description;
}
