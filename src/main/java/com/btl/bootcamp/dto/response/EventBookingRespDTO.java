package com.btl.bootcamp.dto.response;

import lombok.Data;

@Data
public class EventBookingRespDTO {

    private String eventBookingId;
    private String eventBookingDate;
    private String eventType;
    private String description;
}
