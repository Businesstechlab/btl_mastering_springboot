package com.btl.bootcamp.dto.response;

import lombok.Data;

import java.util.UUID;

@Data
public class YogaUserRespDTO {

    private UUID userId;

    private String username;
    private String password;
    private String email;
    private String phoneNumber;
    private String lastUpdatedDate;

    private int isDeleted;
}
