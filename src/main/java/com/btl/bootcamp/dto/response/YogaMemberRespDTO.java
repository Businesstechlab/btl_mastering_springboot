package com.btl.bootcamp.dto.response;

import lombok.Data;

@Data
public class YogaMemberRespDTO {

    private String memberId;
    private String memberName;
    private String password;
    private String email;
    private String phoneNumber;
    private String lastUpdatedDate;
    private int isDeleted;
}
